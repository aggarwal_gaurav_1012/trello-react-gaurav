# Trello project

- This project is based on react with material UI as a react UI framework.

- The API requests are implemented using axios.

- It includes the following features:

    1. For boards:

        - Display Boards

        - Create new board

        - Delete board

    2. For lists:

        - Display all lists in a board
        
        - Create a new list

        - Delete/Archive a list

    3. For cards:

        - Display all cards in a list

        - Create a new card in a list

        - Delete a card

    4. For checklists:

        - Display all checklists in a card

        - Create a checklist in a card

        - Delete a checklist

    5. For checkitems:

        - Display all checkitems
        
        - Create a new checkitem in a checklist
        
        - Delete a checkitem
        
        - Check a checkitem
        
        - Uncheck a checkitem