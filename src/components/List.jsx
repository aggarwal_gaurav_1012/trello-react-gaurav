import React, { useState, useEffect } from 'react';
import { Box, Typography, Button, IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import api from '../api';
import Card from './Card';
import CreateCardForm from './CreateCardForm';

const List = ({ list, setLists }) => {
    const [cards, setCards] = useState([]);
    const [showCardForm, setShowCardForm] = useState(false);

    useEffect(() => {
        const fetchCards = async () => {
            try {
                const response = await api.get(`/lists/${list.id}/cards`);
                setCards(response.data);
            } catch (error) {
                console.error('Error in fetching cards:', error);
            }
        };
        fetchCards();
    }, [list.id]);

    const handleDeleteList = async () => {
        try {
            await api.put(`/lists/${list.id}/closed`, { value: true });
            setLists((prevLists) => prevLists.filter((l) => l.id !== list.id));
        } catch (error) {
            console.error('Error deleting list:', error);
        }
    };

    return (
        <Box bgcolor="grey.700" p={2} borderRadius={2} minWidth={240}>
            <Box display="flex" justifyContent="space-between" alignItems="flex-start">
                <Typography variant="h6">{list.name}</Typography>
                <IconButton onClick={handleDeleteList} size="small" color="secondary">
                    <DeleteIcon />
                </IconButton>
            </Box>
            <Box mt={2}>
                {cards.map((card) => (
                    <Card key={card.id} card={card} setCards={setCards} />
                ))}
                <Button style={{ marginTop: 20 }} variant="contained" color="primary" onClick={() => setShowCardForm(!showCardForm)}>
                    + Add Card
                </Button>
                {showCardForm && <CreateCardForm listId={list.id} setCards={setCards} setShowCardForm={setShowCardForm} />}
            </Box>
        </Box>
    );
};

export default List;