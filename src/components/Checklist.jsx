import React, { useState, useEffect } from 'react';
import { Box, Typography, IconButton, Button, TextField } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import api from '../api';
import CheckItem from './Checkitem';
import LinearProgressWithLabel from './LinearProgressWithLabel';

const Checklist = ({ checklist, setChecklists, cardId }) => {
    const [checkItems, setCheckItems] = useState([]);
    const [newCheckItemName, setNewCheckItemName] = useState('');

    useEffect(() => {
        const fetchCheckItems = async () => {
            try {
                const response = await api.get(`/checklists/${checklist.id}/checkItems`);
                setCheckItems(response.data);
            } catch (error) {
                console.error('Error in fetching check items:', error);
            }
        };
        fetchCheckItems();
    }, [checklist.id]);

    const handleDeleteChecklist = async () => {
        try {
            await api.delete(`/cards/${checklist.idCard}/checklists/${checklist.id}`);
            setChecklists(prevChecklists => prevChecklists.filter(c => c.id !== checklist.id));
        } catch (error) {
            console.error('Error in deleting checklist:', error);
        }
    };

    const handleCreateCheckItem = async (e) => {
        e.preventDefault();
        try {
            const response = await api.post(`/checklists/${checklist.id}/checkItems`, { name: newCheckItemName });
            setCheckItems([...checkItems, response.data]);
            setNewCheckItemName('');
        } catch (error) {
            console.error('Error in creating check item:', error);
        }
    };

    const completedItems = checkItems.filter(item => item.state === 'complete').length;
    const progress = checkItems.length > 0 ? (completedItems / checkItems.length) * 100 : 0;

    return (
        <Box mt={2} p={2} borderRadius={2}>
            <Box display="flex" justifyContent="space-between" alignItems="center">
                <Typography variant="h6">{checklist.name}</Typography>
                <IconButton onClick={handleDeleteChecklist} size="small" color="secondary">
                    <DeleteIcon />
                </IconButton>
            </Box>
            <LinearProgressWithLabel value={progress} />
            <Box mt={2}>
                {checkItems.map(checkItem => (
                    <CheckItem key={checkItem.id} checkItem={checkItem} setCheckItems={setCheckItems} cardId={cardId} />
                ))}
                <Box component="form" onSubmit={handleCreateCheckItem} display="flex" mt={1} gap={1}>
                    <TextField
                        label="New Check Item"
                        variant="outlined"
                        value={newCheckItemName}
                        onChange={(e) => setNewCheckItemName(e.target.value)}
                    />
                    <Button type="submit" variant="contained" color="primary">Add</Button>
                </Box>
            </Box>
        </Box>
    );
};

export default Checklist;