import React from 'react';
import CreateForm from './CreateForm';

const CreateCardForm = ({ listId, setCards, setShowCardForm }) => {
    return (
        <CreateForm
            endpoint='/cards'
            params={{ idList: listId }}
            buttonText='CREATE'
            setItems={setCards}
            setShowForm={setShowCardForm}
        />
    );
};

export default CreateCardForm;