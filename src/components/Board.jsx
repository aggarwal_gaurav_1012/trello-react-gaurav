import React from 'react';
import { Card, CardContent, CardActions, Button, Typography } from '@mui/material';

const Board = ({ board, setBoards }) => {
    return (
        <Card style={{ width: 240, height: 160, display: 'flex', flexDirection: 'column', justifyContent: 'space-between', marginLeft: 25 }}>
            <CardContent>
                <Typography variant="h6">{board.name}</Typography>
            </CardContent>
            <CardActions style={{ justifyContent: 'space-between' }}>
                <Button size="small" href={`/boards/${board.id}`}>OPEN</Button>
            </CardActions>
        </Card>
    );
};

export default Board;