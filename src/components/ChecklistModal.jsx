import React, { useState, useEffect } from 'react';
import { Modal, Box, Typography, Button, IconButton, TextField } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import api from '../api';
import Checklist from './Checklist';

const ChecklistModal = ({ cardId, open, onClose }) => {
    const [checklists, setChecklists] = useState([]);
    const [newChecklistName, setNewChecklistName] = useState('');

    useEffect(() => {
        if (open) {
            const fetchChecklists = async () => {
                try {
                    const response = await api.get(`/cards/${cardId}/checklists`);
                    setChecklists(response.data);
                } catch (error) {
                    console.error('Error in fetching checklists:', error);
                }
            };
            fetchChecklists();
        }
    }, [open, cardId]);

    const handleCreateChecklist = async () => {
        try {
            const response = await api.post(`/cards/${cardId}/checklists`, { name: newChecklistName });
            setChecklists([...checklists, response.data]);
            setNewChecklistName('');
        } catch (error) {
            console.error('Error in creating checklist:', error);
        }
    };

    return (
        <Modal open={open} onClose={onClose}>
            <Box sx={{ p: 4, bgcolor: 'background.paper', borderRadius: 2, maxWidth: 600, margin: 'auto', mt: 5 }}>
                <Box display="flex" justifyContent="space-between" alignItems="center">
                    <Typography variant="h6">Checklists</Typography>
                    <IconButton onClick={onClose}>
                        <CloseIcon />
                    </IconButton>
                </Box>
                {checklists.map(checklist => (
                    <Checklist key={checklist.id} checklist={checklist} setChecklists={setChecklists} cardId={cardId} />
                ))}
                <Box display="flex" mt={2} gap={1}>
                    <TextField
                        label="New Checklist"
                        variant="outlined"
                        value={newChecklistName}
                        onChange={(e) => setNewChecklistName(e.target.value)}
                    />
                    <Button variant="contained" color="primary" onClick={handleCreateChecklist}>Add</Button>
                </Box>
            </Box>
        </Modal>
    );
};

export default ChecklistModal;