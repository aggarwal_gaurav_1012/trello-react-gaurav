import React from 'react';
import CreateForm from './CreateForm';

const CreateBoardForm = ({ setBoards, setShowForm }) => {
    return (
        <CreateForm
            endpoint='/boards'
            params={{}}
            buttonText='CREATE'
            setItems={setBoards}
            setShowForm={setShowForm}
        />
    );
};

export default CreateBoardForm;