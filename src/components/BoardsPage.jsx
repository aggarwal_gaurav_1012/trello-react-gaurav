import React, { useState, useEffect } from 'react';
import { Container, Box, Typography, Button } from '@mui/material';
import { useParams, useNavigate } from 'react-router-dom';
import api from '../api';
import List from './List';
import CreateListForm from './CreateListForm';

const BoardsPage = () => {
    const { boardId } = useParams();
    const navigate = useNavigate();
    const [board, setBoard] = useState(null);
    const [lists, setLists] = useState([]);
    const [showForm, setShowForm] = useState(false);

    useEffect(() => {
        const fetchBoardDetails = async () => {
            try {
                const boardResponse = await api.get(`/boards/${boardId}?fields=name`);
                setBoard(boardResponse.data);

                const listsResponse = await api.get(`/boards/${boardId}/lists`);
                setLists(listsResponse.data);
            } catch (error) {
                console.error('Error in fetching board details:', error);
            }
        };
        fetchBoardDetails();
    }, [boardId]);

    return (
        <div style={{ display: 'flex', backgroundColor: '#121212', color: '#fff' }}>
            <Container style={{ padding: '1.5rem' }}>
                <Box display="flex" justifyContent="space-between" alignItems="center" width={1450}>
                    <Typography variant="h4" style={{ marginTop: 10, fontWeight: 'bold' }}>
                        {board?.name}
                    </Typography>
                    <Button variant="contained" color="primary" onClick={() => navigate('/')}>
                        Home
                    </Button>
                </Box>
                <Box style={{ display: 'flex', alignItems: 'flex-start', overflowX: 'auto', gap: '16px', marginTop: '16px', width: 'max-content' }}>
                    {lists.map((list) => (
                        <List key={list.id} list={list} setLists={setLists} />
                    ))}
                    <Box>
                        <Button variant="contained" color="primary" onClick={() => setShowForm(!showForm)}>
                            + Add another list
                        </Button>
                        {showForm && <CreateListForm boardId={boardId} setLists={setLists} setShowForm={setShowForm} />}
                    </Box>
                </Box>
            </Container>
        </div>
    );
};

export default BoardsPage;