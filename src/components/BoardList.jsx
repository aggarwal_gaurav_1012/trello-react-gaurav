import React, { useState } from 'react';
import { Grid, Box, Button, Typography } from '@mui/material';
import CreateBoardForm from './CreateBoardForm';
import Board from './Board';

const BoardList = ({ boards, setBoards }) => {
    const [showForm, setShowForm] = useState(false);

    return (
        <Box>
            <Typography variant="h5" style={{ marginTop: 30, marginBottom: 30, marginLeft: 25}}>
                Your boards:
            </Typography>
            <Grid container spacing={2}>
                {boards.map(board => (
                    <Grid item key={board.id}>
                        <Board board={board} setBoards={setBoards} />
                    </Grid>
                ))}
                <Grid item>
                    <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        width={240}
                        height={160}
                        bgcolor="grey.800"
                        borderRadius={2}
                        onClick={() => setShowForm(!showForm)}
                        style={{ cursor: 'pointer' }}
                        marginLeft={3}
                    >
                        <Button color="primary" variant="contained">CREATE NEW BOARD</Button>
                    </Box>
                </Grid>
            </Grid>
            {showForm && <CreateBoardForm setBoards={setBoards} setShowForm={setShowForm} />}
        </Box>
    );
};

export default BoardList;