import React from 'react';
import CreateForm from './CreateForm';

const CreateListForm = ({ boardId, setLists, setShowForm }) => {
    return (
        <CreateForm
            endpoint='/lists'
            params={{ idBoard: boardId }}
            buttonText='CREATE'
            setItems={setLists}
            setShowForm={setShowForm}
        />
    );
};

export default CreateListForm;