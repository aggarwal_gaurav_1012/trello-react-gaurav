import React from 'react';
import { Box, Typography, IconButton, Checkbox } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import api from '../api';

const CheckItem = ({ checkItem, setCheckItems, cardId }) => {
    const handleDeleteCheckItem = async () => {
        try {
            await api.delete(`/checklists/${checkItem.idChecklist}/checkItems/${checkItem.id}`);
            setCheckItems(prevCheckItems => prevCheckItems.filter(item => item.id !== checkItem.id));
        } catch (error) {
            console.error('Error in deleting check item:', error);
        }
    };

    const handleCheckItem = async () => {
        try {
            const newState = checkItem.state === 'complete' ? 'incomplete' : 'complete';
            await api.put(`/cards/${cardId}/checkItem/${checkItem.id}?state=${newState}`, { state: newState });
            setCheckItems(prevCheckItems =>
                prevCheckItems.map(item =>
                    item.id === checkItem.id ? { ...item, state: newState } : item
                )
            );
        } catch (error) {
            console.error('Error in checking/unchecking item:', error);
        }
    };

    return (
        <Box display="flex" justifyContent="space-between" alignItems="center" mt={1} p={1} borderRadius={1}>
            <Box display="flex" alignItems="center">
                <Checkbox
                    checked={checkItem.state === 'complete'}
                    onChange={handleCheckItem}
                />
                <Typography>{checkItem.name}</Typography>
            </Box>
            <IconButton onClick={handleDeleteCheckItem} size="small" color="secondary">
                <DeleteIcon />
            </IconButton>
        </Box>
    );
};

export default CheckItem;