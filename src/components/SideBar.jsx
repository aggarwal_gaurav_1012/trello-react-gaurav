import React from 'react';
import { Drawer, List, ListItem, ListItemText, ListItemSecondaryAction, IconButton, Typography } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import { useNavigate } from 'react-router-dom';
import api from '../api';
import logo from '../images/trello_logo.png';

const Sidebar = ({ boards, setBoards }) => {
    const navigate = useNavigate();

    const handleDeleteBoard = async (boardId) => {
        try {
            await api.delete(`/boards/${boardId}`);
            setBoards(prevBoards => prevBoards.filter(board => board.id !== boardId));   
            navigate('/');         
        } catch (error) {
            console.error('Error in deleting board:', error);
        }
    };

    return (
        <Drawer
            variant="permanent"
            PaperProps={{ style: { backgroundColor: '#424242', color: '#fff', width: 300 } }}
        >
            <List>
                <img src={logo} style={{ width: 280, height: 90, marginTop: 10 }} alt="trello" />
                <Typography variant='h6' style={{ margin: '13px', fontWeight: 'bold' }}>Your boards:</Typography>
                {boards.map(board => (
                    <ListItem button component="a" href={`/boards/${board.id}`} key={board.id}>
                        <ListItemText primary={board.name} />
                        <ListItemSecondaryAction>
                            <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteBoard(board.id)}>
                                <DeleteIcon style={{ color: '#fff' }} />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                ))}
            </List>
        </Drawer>
    );
};

export default Sidebar;