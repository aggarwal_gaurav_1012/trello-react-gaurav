import React, { useState } from 'react';
import { Box, Typography, IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import api from '../api';
import ChecklistModal from './ChecklistModal';

const Card = ({ card, setCards }) => {
    const [showChecklistModal, setShowChecklistModal] = useState(false);

    const handleDeleteCard = async () => {
        try {
            await api.delete(`/cards/${card.id}`);
            setCards((prevCards) => prevCards.filter((c) => c.id !== card.id));
        } catch (error) {
            console.error('Error in deleting card:', error);
        }
    };

    return (
        <Box bgcolor="grey.800" p={2} borderRadius={2} display="flex" justifyContent="space-between" alignItems="center" marginBottom={2}>
            <Typography onClick={() => setShowChecklistModal(true)}>{card.name}</Typography>
            <IconButton onClick={handleDeleteCard} size="small" color="secondary">
                <DeleteIcon />
            </IconButton>
            {showChecklistModal && <ChecklistModal cardId={card.id} open={showChecklistModal} onClose={() => setShowChecklistModal(false)} />}
        </Box>
    );
};

export default Card;