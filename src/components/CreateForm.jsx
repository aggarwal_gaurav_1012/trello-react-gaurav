import React, { useState } from 'react';
import { Box, TextField, Button } from '@mui/material';
import api from '../api';

const CreateForm = ({ endpoint, params, buttonText, setItems, setShowForm }) => {
    const [name, setName] = useState('');

    const handleCreate = async (e) => {
        e.preventDefault();
        try {
            const response = await api.post(endpoint, { name, ...params });
            setItems(prevItems => [...prevItems, response.data]);
            setShowForm(false);
        } catch (error) {
            console.error(`Error in creating item at ${endpoint}:`, error);
        }
    };

    return (
        <Box mt={2} component="form" onSubmit={handleCreate} display="flex" gap={2}>
            <TextField
                label="Name"
                variant="outlined"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
            />
            <Button type="submit" variant="contained" color="primary">{buttonText}</Button>
        </Box>
    );
};

export default CreateForm;