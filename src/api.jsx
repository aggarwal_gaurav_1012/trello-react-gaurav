import axios from 'axios';

const apiKey = '302d0bd906ba164038364ba764f6f413';
const apiToken = 'ATTA71b895ec11b616f6ad63f8a7ca27a65db6ee722415ffaf86bd7665401ad3e9b47ADD2EE5';

const api = axios.create({
    baseURL: 'https://api.trello.com/1',
    params: {
        key: apiKey,
        token: apiToken
    }
});

export default api;