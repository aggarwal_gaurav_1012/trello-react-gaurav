import React, { useState, useEffect } from 'react';
import { Container, CssBaseline, Typography } from '@mui/material';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import BoardList from './components/BoardList';
import Sidebar from './components/SideBar';
import BoardsPage from './components/BoardsPage';
import api from './api';

const App = () => {
    const [boards, setBoards] = useState([]);

    useEffect(() => {
        const fetchBoards = async () => {
            try {
                const response = await api.get('/members/me/boards', { params: { fields: 'name,url' } });
                setBoards(response.data);
            } catch (error) {
                console.error('Error in fetching boards: ', error);
            }
        };
        fetchBoards();
    }, []);

    return (
        <Router>
            <CssBaseline />
            <Container maxWidth="lg" style={{ marginLeft: 350 }} >
                <Typography variant="h4" component="h1" style={{ padding: 20, marginTop: 10 }} >
                    Gaurav Aggarwal's workspace
                </Typography>
                <Sidebar boards={boards} setBoards={setBoards} />
                <Routes>
                    <Route path="/" element={<BoardList boards={boards} setBoards={setBoards} />} />
                    <Route path="/boards/:boardId" element={<BoardsPage />} />
                </Routes>
            </Container>
        </Router>
    );
};

export default App;